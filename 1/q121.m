clear;
image = imread("barbara.bmp");
image = rgb2gray(image);

row = size(image,1);
column = size(image,2);

result = uint8(zeros(row,column));

greyLevel =uint8(zeros(1,8));

reduceCount = 256/128;

for i=1:row
    for j=1:column
        temp = round(image(i,j)/reduceCount);
        temp = ((temp * reduceCount) + reduceCount)/2;
        result(i,j) = round(temp);
    end
end

result2 = histeq(result);


imwrite(result,"128level.jpg")
imwrite(result2,"128levelh.jpg")