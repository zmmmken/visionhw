clear
clc
close

image = imread("Goldhill.bmp");

removeRow = uint8(ones(size(image)/2));

%first remove row and column
for i=1:2:size(image,1)
    for j=1:2:size(image,1)
        removeRow(ceil(i/2),ceil(j/2)) = image(i,j);
    end
end


%average filter for reduce frequency of picture
w=ones(2);
w=w/sum(w(:));
img2=imfilter(image,w,'circular');
removeRow2 = uint8(ones(size(image)/2));
for i=1:2:size(img2,1)
    for j=1:2:size(img2,1)
        removeRow2(ceil(i/2),ceil(j/2)) = img2(i,j);
    end
end
clear img2

%pixel replication for without average filter
result1 = pixelReplication(removeRow);
result2 = pixelReplication(removeRow2);
result3=bilinear(removeRow,2);
result4=bilinear(removeRow2,2);

imwrite(result1,"pixelReplicationWithOutAverage.jpg");
imwrite(result2,"pixelReplicationWithAverage.jpg");
imwrite(result3,"bilinearWithOutAverage.jpg");
imwrite(result4,"bilinearWithAverage.jpg");


figure;

subplot(2,2,1);
imshow(image);
title('Original Image');

subplot(2,2,2);
imshow(result1);
title('pixelReplication without filter');

subplot(2,2,3);
imshow(result2);
title('pixelReplication with filter');





function res = pixelReplication(x)
    res = uint8(ones((size(x)*2)));
    for i=1:size(x,1)
        for j=1:size(x,2)
            res(i*2,j*2) = x(i,j);
            res((i*2)-1,(j*2)-1) = x(i,j);
        end
    end
end

function res = bilinear(input,factor)
    res = uint8(zeros(size(input)*(factor)));
    
    for i=factor:size(res,1)-factor
        for j=factor:size(res,2) - factor
            x1 = floor(i/factor);
            y1 = floor(j/factor);
            
            x2 = x1+1;
            y2 = y1 +1;
         
            x22 = x2 * factor;
            x11 = x1 * factor;
            y11 = y1 * factor;
            y22  = y2 * factor;
            
            
            dr1 = (j - y11)/factor;
            dr2 = (y22 -j)/factor;
            
            dc1 = (i -x11)/factor;
            dc2 = (x22 - i)/factor;
            
            temp =  input(x1,y1)*dr2*dc2 +...
                    input(x2,y1)*dr2*dc1+...
                    input(x1,y2)*dr1*dc2+...
                    input(x2,y2)*dr1*dc1;
            
            res(i,j) = round(temp);
            
        end
    end
    
end
