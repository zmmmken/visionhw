



image = imread("Car1.jpg");
movImage = imread("Car2.jpg");



mPoints = [103.333333333333,426.666666666667;124.333333333333,411.666666666667;142.666666666667,423;35.6666666666667,432.666666666667;145,448.666666666667;134.666666666667,515.666666666667;341,481.333333333333;202.333333333333,471.666666666667;237.333333333333,443;274.666666666667,467.333333333333;292.666666666667,471;260,514.666666666667];
fPoints = [527.333333333333,409.333333333333;546.666666666667,393.000000000000;564.333333333333,406.333333333333;466.666666666667,414.000000000000;567.000000000000,429.333333333333;557.666666666667,492.666666666667;758.666666666667,464.333333333333;621.000000000000,451.666666666667;654.666666666667,423.666666666667;691.666666666667,448.000000000000;710.666666666667,452.333333333333;678,495.666666666667];

%mPoints = [103,426;124,411;142,423;35,432;145,448;134,515;341,481;202,471;237,443;274,467;292,471;260,514];
%fPoints = [527,409;546,393;564,406;466,414;567,429;557,492;758,464;621,451;654,423;691,448;710,452;678,495];

syms c1 c2 c3 c4 c5 c6 c7 c8

eq1 = mPoints(2,1)==c1+c2 * fPoints(2,1) + c3*fPoints(2,2) + c4 * fPoints(2,2)*fPoints(2,1);
eq2 = mPoints(2,2)==c5+c6 * fPoints(2,1) + c7*fPoints(2,2) + c8 * fPoints(2,2)*fPoints(2,1);

eq3 = mPoints(5,1)==c1+c2 * fPoints(5,1) + c3*fPoints(5,2) + c4 * fPoints(5,2)*fPoints(5,1);
eq4 = mPoints(5,2)==c5+c6 * fPoints(5,1) + c7*fPoints(5,2) + c8 * fPoints(5,2)*fPoints(5,1);

eq5 = mPoints(9,1)==c1+c2 * fPoints(9,1) + c3*fPoints(9,2) + c4 * fPoints(9,2)*fPoints(9,1);
eq6 = mPoints(9,2)==c5+c6 * fPoints(9,1) + c7*fPoints(9,2) + c8 * fPoints(9,2)*fPoints(9,1);

eq7 = mPoints(12,1)==c1+c2 * fPoints(12,1) + c3*fPoints(12,2) + c4 * fPoints(12,2)*fPoints(12,1);
eq8 = mPoints(12,2)==c5+c6 * fPoints(12,1) + c7*fPoints(12,2) + c8 * fPoints(12,2)*fPoints(12,1);


res = solve(eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8);

a= double(res.c1);
b= double(res.c2);
c= double(res.c3);
d= double(res.c4);

e= double(res.c5);
f= double(res.c6);
g= double(res.c7);
h= double(res.c8);


result = uint8(zeros(7000,7000,3));

for i=1:750
    for j=1:1000
        result(i+5000-375,j+5000-500,:) =image(i,j,:);
    end
end

for i=1:750
    for j=1:1000
        
        x = a + b* i + c* j + d*i * j + 5000;
        y = e + f * i + g * j + h*i * j +5000;
        
        %x = round(transform(1,1)) + 1500;
        %y = round(transform(1,2)) + 1500;
        
        result(round(x),round(y),:) = movImage(i,j,:); 
    end
end

imwrite(result,"panorama.jpg")
%imshow(result,'Interpolation','bilinear')
