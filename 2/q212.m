clear
close all
clc

image  = imread("Camera Man.bmp");
res = histogramEqualization(image);
matlabHist = histeq(image);

figure;
subplot(2,3,1);
imshow(image);
title("Orginal Image");

subplot(2,3,2);
imshow(res);
title("Enhance with histEq");

subplot(2,3,3);
imshow(matlabHist);
title("Enhance with matlab histEq function");

%Draw histogram
subplot(2,3,4);
drawHistogram(image);
title("histogram Orginal Image");

subplot(2,3,5);
drawHistogram(res);
title("histogram Enhance with histEq");

subplot(2,3,6);
drawHistogram(matlabHist);
title("histogram Enhance with matlab histEq function");

