clear
close all
clc

image  = imread("Camera Man.bmp");

subplot(3,2,1);
imshow(image);
title("Orginal Image");

subplot(3,2,2);
drawHistogram(image);
title("orginal histogram")


subplot(3,2,3);
imshow(histeq(image));
title("histeq Image");

subplot(3,2,4);
drawHistogram(histeq(image));
title("histEq histogram")

subplot(3,2,5);
imshow(imadjust(image));
title("ImAdjust Image")

subplot(3,2,6);
drawHistogram(imadjust(image));
title("ImAdjust Histogram")

figure
subplot(1,2,1);
imshow(imadjust(image,[0.2 0.8] , [0.4 0.6], 2));
title("ImAdjust IO: [0.2 0.8] , [0.4 0.6] gama=2")

subplot(1,2,2);
drawHistogram(imadjust(image,[0.2 0.8] , [0.4 0.6],2));
title("ImAdjust Histogram gama=2")

