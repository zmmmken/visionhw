function res = bilinear(input,factor)
    res = uint8(zeros(size(input)*(factor)));
    
    for i=factor:size(res,1)-factor
        for j=factor:size(res,2) - factor
            x1 = floor(i/factor);
            y1 = floor(j/factor);
            
            x2 = x1+1;
            y2 = y1 +1;
         
            x22 = x2 * factor;
            x11 = x1 * factor;
            y11 = y1 * factor;
            y22  = y2 * factor;
            
            
            dr1 = (j - y11)/factor;
            dr2 = (y22 -j)/factor;
            
            dc1 = (i -x11)/factor;
            dc2 = (x22 - i)/factor;
            
            temp =  input(x1,y1)*dr2*dc2 +...
                    input(x2,y1)*dr2*dc1+...
                    input(x1,y2)*dr1*dc2+...
                    input(x2,y2)*dr1*dc1;
            
            res(i,j) = round(temp);
            
        end
    end
    
end