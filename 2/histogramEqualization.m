function result=histogramEqualization(input)
    counter = divideInGrayScale(input);
    [x , y] = size(input);
    result = uint8(zeros(x,y));
    count = x*y;
    possibility = counter / count;
    sum(possibility(1,:))
    histo = zeros(1,256);
    mapMatrix = zeros(1,255);
    summ = 0;
    for i=1:256
        summ = 255 * possibility(1,i) + summ;
        temp = round(summ);
        mapMatrix(1,i) = temp;
        histo(1,temp+1)= counter(1,i) + histo(1,temp+1);
    end
    
    
    for i=1:x
        for j=1:y 
           result(i,j) = mapMatrix(1,input(i,j)+1);
        end
    end
    
end