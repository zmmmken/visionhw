function drawHistogram(image)
    counter = divideInGrayScale(image);
    x = 0:1:255;
    bar(x,counter(1,:))
end