clc;
clear;
close all;

image1  = imread("HE4.jpg");

result256 = LocalHistogram(rgb2gray(image1),256);
result128 = LocalHistogram(rgb2gray(image1),128);
result64 = LocalHistogram(rgb2gray(image1),64);



subplot(2,4,1);
imshow(image1);

subplot(3,4,2);
imshow(result64);

subplot(3,4,3);
imshow(result128);

subplot(2,4,4);
imshow(result256);

subplot(2,4,5)
drawHistogram(image1)
title("orginalImage")

subplot(2,4,6)
drawHistogram(result64)
title("bloc size 64")

subplot(2,4,7)
drawHistogram(result128)
title("bloc size 128")

subplot(2,4,8)
drawHistogram(result256)
title("bloc size 256")

figure

subplot(2,2,1);
imshow(image1);
subplot(2,2,2);
drawHistogram(image1);
result = histeq(image1);
subplot(2,2,3);
imshow(result);
subplot(2,2,4);
drawHistogram(result);



function res = LocalHistogram(input,blocSize)
   [x,y] = size(input);
   XmaxRange = blocSize * (floor(x/blocSize));
   YmaxRange = blocSize * (floor(y/blocSize));
  
   for i=1:blocSize:XmaxRange
    for j=1:blocSize:YmaxRange
        res(i:i+blocSize-1,j:j+blocSize-1) = histogramEqualization(input(i:i+blocSize-1,j:j+blocSize-1));
    end
   end
end