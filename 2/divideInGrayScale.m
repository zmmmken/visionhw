function counter=divideInGrayScale(input)
    [x ,y] = size(input);
    counter = zeros(1,256);
    for i=1:x
        for j=1:y
            temp = input(i,j);
            counter(1,temp+1) = counter(1,temp+1) + 1; 
        end
    end
end